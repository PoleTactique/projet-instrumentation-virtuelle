############################################################################
#					AUTHOR: Alvaro MORENA BERODAS						   #
############################################################################

############################################################################
#																		   #
#			    ACQUISITION DES DONNEES DE L'OSCILLOSCOPE	               #
#																		   #
############################################################################

############################################################################
#								inputs									   #
############################################################################
#appOsc = rm.open_resource('GPIB0::5::INSTR')						   #
#NP=Nombre maximum de points											   #
#select_curves=determine les courbes a copier							   #
############################################################################

############################################################################
#								outputs									   #
############################################################################
#						[thoff,thint,values]							   #
#thoff= offset (position temporelle du point 0)							   #
#thint= intervalle entre deux points (en s)								   #
#values= valeur de la courbe(amplitude)									   #
############################################################################

def acquisitionsimple(appOsc,outputname='Output',coupling='D50',select_curve='C1',NP='50000'): #Pour une seule courbe, Retourne [t offset, t interval, string avec la courbe]
	print('=====================')
	# select_curve=['C1']
	#=========================================================================================
	#						SETUP POUR L'ACQUISITION DES DONNÉES
	#=========================================================================================

	# NP='50000'
	SP='0'
	FP='0'

	appOsc.write('WFSU SP,'+SP+',NP,'+str(NP)+',FP,'+FP)# appOsc.write('WFSU SP,0,NP,30000,FP,0')#Setup des données. 'WFSU SP,a,NP,b,FP,c'
										   #					SP(Sparsing parameter)=a: définition de l'intervalle entre points. SP=0 envoie tous les points//SP=4 récupère des échantillons tous les 4 points.
										   #					NP(number of points)=b: définition du nombre max des points a récupérer.
										   #					FP(First Point)=c: établit l'adresse du premier point a échantilloner
										   
	#=========================================================================================

	appOsc.write("COUPLING "+coupling) #Permet d'établir l'impédance d'entrée à 50 Ohm
								 #appOsc.write("C1:COUPLING A1M") Dans ce cas on modifie l'impédance pour le canal 1
								 #appOsc.write("C1:COUPLING D1M")
								 #appOsc.write("C1:COUPLING GND")
								 #appOsc.write("C1:COUPLING OVL")

	import time, datetime

	d = datetime.datetime.now()
	# print(time.mktime(d.timetuple()))

	date=str(time.mktime(d.timetuple()))
	date=date.replace('.0','')

	import datetime
	today = datetime.date.today()
	# print(today)
	today = str(today)

	print(time.strftime("%H:%M:%S"))
	
	date = today +'-'+ date + '-'
	print(date)

#========================
# import time, datetime
# d1 = datetime.datetime.now()
# t1=time.mktime(d1.timetuple())
# data1 = appOsc.query(Name+':INSPECT? "SIMPLE"')
# d2 = datetime.datetime.now()
# t2=time.mktime(d2.timetuple())
# print('Temps de acquisition: '+str(t2-t1)+' s')
#========================

	#=========================================================================================
	#						ACQUISITION DES DONNÉES
	#=========================================================================================
	
	# appOsc.write('STOP')
	# print('STOP:.......................OK')
	
	# print(select_curves)
	


	from time import time
	Name = select_curve
	print(select_curve)
	t1=time()
	data1 = appOsc.query(Name+':INSPECT? "SIMPLE"')
	timebase1 = appOsc.query(Name+':INSPECT? "TIMEBASE"')
	t2=time()
	print('Temps de acquisition: '+str(t2-t1)+' s')
	#===========================================================================================
	#									TRAITEMENT DES DONNÉES
	#===========================================================================================
	import matplotlib.pyplot as plt


	#Enregistrement des valeurs individuels
	values = []
	values += data1.split()	
	values.pop(0)
	values.pop(1)
	values.pop(len(values)-1)
	
	thuni=appOsc.query("INSPECT? HORUNIT")
	thuni=thuni.replace(Name+':INSP "HORUNIT            : Unit Name = ','')
	thuni=thuni.replace('"','')
	thuni=thuni.replace('\n','')
	
	#Plot de la courbe
	xLen = len(values)
	fig = plt.figure()
	plt.ylabel('Voltage (V)')
	plt.xlabel('Time')
	fig.canvas.set_window_title("Oscilloscope")
	plt.plot(range(xLen), values)

	plotname = date+Name+".png"
	plt.savefig(plotname)

	print('PNG SAVED AS:'+plotname)
	import numpy
	from numpy import array

	thoff=appOsc.query("INSPECT? HORIZ_OFFSET")
	thint=appOsc.query("INSPECT? HORIZ_INTERVAL")


	thoff=thoff.replace(Name+':INSP "HORIZ_OFFSET       : ','')
	thoff=thoff.replace('"','')
	thoff=thoff.replace('\n','')
	thint=thint.replace(Name+':INSP "HORIZ_INTERVAL     : ','')
	thint=thint.replace('"','')
	thint=thint.replace('\n','')
	
	t=[thoff,thint]
	temp=array(t).astype("float")
	# print(temp)

	a=temp[0]
	b=temp[1]
	time=[]
	# for i in range(len(values)):
		# time.insert(i,a+i*b)
	
	if SP=='0':
		for i in range(len(values)):
			time.insert(i,a+i*b)
	else:
		for i in range(len(values)):
			time.insert(i,a+int(SP)*i*b)
	#===========================================================================================
	filenamedat_curve = date+Name+'_curve.dat'
	file3 = open(filenamedat_curve, "w")

	timestr=[]
	for i in range(len(values)):
		timestr.insert(i,str(time[i]))
	
	file3.write('('+thuni+')')
	file3.write(';')
	file3.write('Amplitude')
	file3.write('\n')
		
	for i in range(len(values)):
		file3.write(timestr[i])
		file3.write(';')
		file3.write(values[i])
		file3.write('\n')
	file3.close()
	print('CURVE (.dat) SAVED AS:'+filenamedat_curve)		
	#========================================	
	# print(thoff)
	# print(thint)
	print('=====================')
	output = [thoff,thint,values]
	appOsc.write('TRIG_MODE AUTO')
	return output
def acquisition(appOsc,select_curves,coupling='D50',outputname='Output',NP='50000',SP='0',FP='0'): #pour plusieurs courbes, enregistre des fichiers avec les courbes
										  #inputs: NP:Nombre de points, select_curves:Selection des courbes a copier, SP: Intervalle entre points, FP: Premier point a copier
										  #Nombre total de points = NP*SP+FP < 100000 avec SP et FP differents de 0. La capacite de l'ecran et donc de capture de l'oscilloscope
										  #est de 100000 points.
	print('=====================')
	# select_curves=['C1','F1,'F2']
	#=========================================================================================
	#						SETUP POUR L'ACQUISITION DES DONNÉES
	#=========================================================================================

	# NP='50000'
	# SP='0'
	# FP='0'

	appOsc.write('WFSU SP,'+str(SP)+',NP,'+str(NP)+',FP,'+str(FP))# appOsc.write('WFSU SP,0,NP,30000,FP,0')#Setup des données. 'WFSU SP,a,NP,b,FP,c'
										   #					SP(Sparsing parameter)=a: définition de l'intervalle entre points. SP=0 envoie tous les points//SP=4 récupère des échantillons tous les 4 points.
										   #					NP(number of points)=b: définition du nombre max des points a récupérer.
										   #					FP(First Point)=c: établit l'adresse du premier point a échantilloner
										   
	#=========================================================================================

	appOsc.write("COUPLING "+str(coupling)) #Permet d'établir l'impédance d'entrée à 50 Ohm
								 #appOsc.write("C1:COUPLING A1M") Dans ce cas on modifie l'impédance pour le canal 1
								 #appOsc.write("C1:COUPLING D1M")
								 #appOsc.write("C1:COUPLING GND")
								 #appOsc.write("C1:COUPLING OVL")
	appOsc.timeout = 20000
	import time, datetime

	d = datetime.datetime.now()
	# print(time.mktime(d.timetuple()))

	date=str(time.mktime(d.timetuple()))
	date=date.replace('.0','')

	import datetime
	today = datetime.date.today()
	# print(today)
	today = str(today)

	date = today +'-'+ date
	hora = time.strftime("%H:%M:%S")
	print(hora)


	#=========================================================================================
	#						ACQUISITION DES DONNÉES
	#=========================================================================================
	if len(select_curves)>1:
		appOsc.write('STOP')
		print('STOP:.......................OK')
	
	# print(select_curves)
	

	for element in select_curves:
		appOsc.timeout = 20000
		# Name = select_curves[1] #Spécifier ici la curve a récuperer de l'oscilloscope. Faire attention au format STRING
		Name = element

		print(Name)
		from time import time
		t1=time()
		data1 = appOsc.query(Name+':INSPECT? "SIMPLE"')
		t2=time()
		print('Temps de acquisition: '+str(t2-t1)+' s')	
		timebase1 = appOsc.query(Name+':INSPECT? "TIMEBASE"')

		values = []
		values += data1.split()	
		values.pop(0)
		values.pop(1)
		values.pop(len(values)-1)



		#===========================================================================================
		#									TRAITEMENT DES DONNÉES
		#===========================================================================================
		import matplotlib.pyplot as plt
		# Ouverture du fichier

		
		thuni=appOsc.query("INSPECT? HORUNIT")
		thuni=thuni.replace(Name+':INSP "HORUNIT            : Unit Name = ','')
		thuni=thuni.replace('"','')
		thuni=thuni.replace('\n','')
		
		# # Plot de la courbe
		# xLen = len(values)
		# fig = plt.figure()
		# plt.ylabel('Voltage (V)')
		# plt.xlabel('Time')
		# fig.canvas.set_window_title("Oscilloscope")
		# plt.plot(range(xLen), values)

		# plotname = date+'-'+Name+".png"
		# plt.savefig(plotname)

		# print('PNG SAVED AS:'+plotname)
		import numpy
		from numpy import array

		thoff=appOsc.query("INSPECT? HORIZ_OFFSET")
		thint=appOsc.query("INSPECT? HORIZ_INTERVAL")


		thoff=thoff.replace(Name+':INSP "HORIZ_OFFSET       : ','')
		thoff=thoff.replace('"','')
		thoff=thoff.replace('\n','')
		thint=thint.replace(Name+':INSP "HORIZ_INTERVAL     : ','')
		thint=thint.replace('"','')
		thint=thint.replace('\n','')
		
		t=[thoff,thint]
		temp=array(t).astype("float")
		# print(temp)

		a=temp[0]
		b=temp[1]
		time=[]

		if str(SP)=='0':
			for i in range(len(values)):
				time.insert(i,a+i*b)
		else:
			for i in range(len(values)):
				time.insert(i,a+int(SP)*i*b)
		# for i in range(len(values)):
			# time.insert(i,a+i*b)
		#===========================================================================================
		filenamedat_curve = outputname+'.dat'
		file3 = open(filenamedat_curve, "w")

		timestr=[]
		for i in range(len(values)):
			timestr.insert(i,str(time[i]))
			
		for i in range(len(values)):
			file3.write(timestr[i])
			file3.write(' ')
			file3.write(values[i])
			file3.write('\n')
		file3.close()
		print('CURVE (.dat) SAVED AS:'+filenamedat_curve)
		#========================================	
		# print(thoff)
		# print(thint)
		print('=====================')
	# appOsc.write('TRIG_MODE AUTO')


############################################################################
#				procedures de communication oscilloscope				   #
############################################################################
		
def Ecriture(nom_fichier,temps,valeur): #enregistre un fichier avec la courbe, inputs: Nom du fichier de sortie (avec terminaison .dat ou .txt), vecteur de temps et vecteur des amplitudes
	file3=open(nom_fichier,"w")
	file3.write('Timebase')
	file3.write(';')
	file3.write('Amplitude')
	file3.write('\n')
	for i in range(len(valeur)):
		file3.write(str(temps[i]))
		file3.write(';')
		file3.write(str(valeur[i]))
		file3.write('\n')
	file3.close()	
	print('FILE SAVED AS:'+nom_fichier)
	
def RepresentY(nom_fichier,valeur,xlabel='#Point',ylabel='Amplitude'): #Represente la courbe en un plot, inputs: Nom du fichier de sortie (avec terminaison .png), vecteur des amplitudes
	import matplotlib.pyplot as plt
	#Plot de la courbe
	xLen = len(valeur)
	fig = plt.figure()
	plt.ylabel(ylabel)
	plt.xlabel(xlabel)
	fig.canvas.set_window_title("Oscilloscope")
	plt.plot(range(xLen), valeur)
	plt.savefig(nom_fichier)
	print('PLOT SAVED AS:'+nom_fichier)

def RepresentXY(nom_fichier,X,Y,xlabel='time (s)',ylabel='Amplitude'): #Represente la courbe en un plot, inputs: Nom du fichier de sortie (avec terminaison .png), vecteur des amplitudes
	import matplotlib.pyplot as plt
	#Plot de la courbe
	xLen = len(Y)
	fig = plt.figure()
	plt.ylabel(ylabel)
	plt.xlabel(xlabel)
	fig.canvas.set_window_title("Oscilloscope")
	plt.plot(X, Y)
	plt.savefig(nom_fichier)
	print('PLOT SAVED AS:'+nom_fichier)
	
def FFT(valeur): #Retourne un vecteur avec la FFT de la courbe. input: Vecteur des amplitudes
	import numpy
	from numpy import array
	input = array(valeur).astype("float")
	return	numpy.fft.fft(input)
	
def autoset(appOsc): #Realise un autoset de l'oscilloscope
	appOsc.write('ASET')

def stop(appOsc): #Pause la prise de données de l'oscilloscope
	appOsc.write('STOP')

def autotrig(appOsc): #Redemarre la prise de données l'oscilloscope
	appOsc.write('TRIG_MODE AUTO')
	
def Tdiv(appOsc,s): #modifie les divisions de temps dans l'oscilloscope
	appOsc.write('TIME_DIV '+s)
	#Il faut inclure les unités de la base de tems en majuscules i.e. 2NS (2ns), 1M (1ms). Input s en format string
	#200 500 					ps
	#1 2 5 10 20 50 100 200 500 ns
	#1 2 5 10 20 50 100 200 500 us
	#1 2 5 10 20 50 100 200 500 ms
	#1 2 5 10 20 50 100 200 500 s
	#1							ks


def parameters(appOsc,Name): #Enregistre les parametre en un fichier .dat et retourne les valeurs dans un string 
							 #Name = String avec le nom de la courbe sur l'oscilloscope i.e 'C1'
	
	import time, datetime

	d = datetime.datetime.now()
	date=str(time.mktime(d.timetuple()))
	date=date.replace('.0','')
	
	import datetime
	today = datetime.date.today()
	# print(today)
	today = str(today)
	
	filenamedat_prop = today+'-'+date+'-'+Name+'_properties.dat'
	file3 = open(filenamedat_prop, "w")
	
	period = appOsc.query(Name+':PAVA? PER')
	period=period.replace(Name+':PAVA PER,','')
	period=period.replace(' S,OK','')
	period=period.replace(' S,NP','')
	period=period.replace('\n','')

	file3.write('Periode (s)')
	file3.write(';')
	file3.write(period)
	file3.write('\n')	
	
	freq = appOsc.query(Name+':PAVA? FREQ')
	freq=freq.replace(Name+':PAVA FREQ,','')
	freq=freq.replace(' HZ,OK','')
	period=period.replace(' HZ,NP','')
	freq=freq.replace('\n','')
	
	file3.write('Frequency (Hz)')
	file3.write(';')
	file3.write(freq)
	file3.write('\n')
	
	max = appOsc.query(Name+':PAVA? MAX')
	max=max.replace(Name+':PAVA MAX,','')
	max=max.replace(' ',';')
	max=max.replace('V,OK','')
	max=max.replace('V,NP','')
	max=max.replace('\n','')
	
	file3.write('Max Value (V)')
	file3.write(';')
	file3.write(max)
	file3.write('\n')
	
	min = appOsc.query(Name+':PAVA? MIN')
	min=min.replace(Name+':PAVA MIN,','')
	min=min.replace(' ',';')
	min=min.replace('V,OK','')
	min=min.replace('V,NP','')
	min=min.replace('\n','')
	
	file3.write('Min Value (V)')
	file3.write(';')
	file3.write(min)
	file3.write('\n')
	
	ampli = appOsc.query(Name+':PAVA? AMPL')
	ampli=ampli.replace(Name+':PAVA AMPL,','')
	ampli=ampli.replace(' ',';')
	ampli=ampli.replace('V,OK','')
	ampli=ampli.replace('V,NP','')
	ampli=ampli.replace('\n','')
	
	file3.write('Amplitude (V)')
	file3.write(';')
	file3.write(ampli)
	file3.write('\n')
	
	mean = appOsc.query(Name+':PAVA? MEAN')
	mean=mean.replace(Name+':PAVA MEAN,','')
	mean=mean.replace(' ',';')
	mean=mean.replace('V,OK','')
	mean=mean.replace('V,NP','')
	mean=mean.replace('\n','')
	
	file3.write('Mean Value (V)')
	file3.write(';')
	file3.write(mean)
	file3.write('\n')
	
	sdeviation = appOsc.query(Name+':PAVA? SDEV')
	sdeviation=sdeviation.replace(Name+':PAVA SDEV,','')
	sdeviation=sdeviation.replace(' ',';')
	sdeviation=sdeviation.replace('V,OK','')
	sdeviation=sdeviation.replace('V,NP','')
	sdeviation=sdeviation.replace('\n','')
	
	file3.write('Standard deviation (V)')
	file3.write(';')
	file3.write(sdeviation)
	file3.write('\n')
	
	point = appOsc.query(Name+':PAVA? PNTS')
	point=point.replace(Name+':PAVA PNTS,','')
	point=point.replace(',OK','')
	point=point.replace(',NP','')
	point=point.replace('\n','')
	
	file3.write('Number of points')
	file3.write(';')
	file3.write(point)
	file3.write('\n')
	
	file3.close()
	print('File(.dat) SAVED AS:'+filenamedat_prop)
	print('Periode: '+period+'s\nFrequence: '+freq+'Hz\nMax: '+max+'\nMin: '+min+'\nAmplitude: '+ampli+'\nMean Value: '+mean+'\nEcart type: '+sdeviation+'#Points'+point)
	return [period,freq,max,min,ampli,mean,sdeviation,point] #Retourne les valeurs en format string
	
def numpoints(appOsc,Name='C1'):

	period = appOsc.query(Name+':PAVA? PER')
	period=period.replace(Name+':PAVA PER,','')
	period=period.replace(' S,OK','')
	period=period.replace(' S,NP','')
	period=period.replace('\n','')
	period = float(period)
	
	thint=appOsc.query("INSPECT? HORIZ_INTERVAL")	
	thint=thint.replace(Name+':INSP "HORIZ_INTERVAL     : ','')
	thint=thint.replace('"','')
	thint=thint.replace('\n','')
	thint = float(thint)
	
	Nperiod = period/thint
	
	Nperiod = int(Nperiod)
	
	return Nperiod
	
def clsweeps(appOsc):
	appOsc.write('CLSW')
import visa
rm = visa.ResourceManager()

"""
def main():
	appOsc = rm.open_resource('USB::0x0AAD::0x01D6::101477::INSTR')
	acquisitionsimple(appOsc)  # Pour une seule courbe, Retourne [t offset, t interval, string avec la courbe]
	print('hello world')

main()
"""
# -*- coding: utf-8 -*-
 
from tkinter import * 
from project_instru import *
from tkinter import messagebox as tkMessageBox

window = Tk()

def show_idn():

    Label(window, text="ID: "+get_idn()).pack(padx=10, pady=10)

def show_head():

    Label(window, text="Xstart,Xstop, record in length sample and channel: "+ get_head()).pack(padx=10, pady=10)


def show_point():
    Label(window, text="Number of Points: "+get_poin()).pack(padx=10, pady=10)

def show_values():
    Label(window, text=get_data()).pack(padx=10, pady=10)


def save_data():
    save_data()


window.title("PROJET INSTRUMENTATION VIRTUELLE")

window.geometry('500x700')

bouton1 = Button(window, text="IDN",command=show_idn)
bouton2 = Button(window, text="HEAD",command=show_head)
bouton3 = Button(window, text="POIN",command=show_point)
bouton4 = Button(window, text="VALUES (not ready)",command=show_values)
bouton5 = Button(window, text="SAVE VALUES TO data.csv (not ready)",command=save_data)
bouton1.pack()
bouton2.pack()
bouton3.pack()
bouton4.pack()
bouton5.pack()
window.mainloop()


# Simple Python script using PyVISA
#
# The script opens a connection to an instrument and queries its identification string
# with a SCPI command '*IDN?'
# Preconditions:
# - R&S VISA or NI VISA installed
# - Python 2.7.13 or newer installed
# - Python PyVISA 1.7 package or newer installed
# - Resource string adjusted to fit your instrument physical connection
from acquisition import *
import numpy as np
import visa
import numpy as np
import matplotlib.pyplot as plt
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.figure_factory as FF
import csv
import pandas as pd

resourceString = 'USB::0x0AAD::0x01D6::101477::INSTR'  # USB-TMC (Test and Measurement Class)
#resourceString = 'USB0::2733::470::101477::0::INSTR'
rm = visa.ResourceManager()
#rm.list_resources()
scope = rm.open_resource(resourceString)
scope.write_termination = '\n'
scope.read_termination = '\n'
scope.timeout = 5000

scope.clear()  # Clear instrument io buffers and status
#idn_response = scope.query('*IDN?')  # Query the Identification string
#print(rm)
#print (idn_response)
#print(scope.query("CHAN:DATA:HEAD?")) #Read header Xstart, Xstop, record length in samples
#print(scope.query("CHAN:DATA:POIN?"))
#curve = scope.query("CHAN:DATA?")
#print(scope.query('FORM?'))
#print(scope.query('SING;*OPC?')) #Start single acquisition
#print(scope.query('PROBe2:SETup:DCOFfset?'))
#print(scope.query('PROB:SET:NAME?'))
#print(curve)


def get_idn():
    return scope.query("*IDN?")
def get_head():
    return scope.query("CHAN:DATA:HEAD?")
def get_poin():
    return scope.query("CHAN:DATA:POIN?")    
def get_data():
    return scope.query('CHAN:DATA?')
def save_data():
    data=""
    line=""

    curve= get_data()
    #print(curve)
    
    for i in curve:
        i=str(i)
        if i!=",":
            line+=i
        if '"'==i:
            data+=line
            line=""


    with open('data.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(data)

    csvFile.close()